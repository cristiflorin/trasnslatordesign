package com.Cristi;

import java.io.*;
import java.util.ArrayList;

public class Main {

    static String tmp="";
    static int count=1;
    static int errorCounter=0;
    static String error="" ;


    public static void main(String[] args) {
	// write your code here

        try{
            // Open the file that is the first
            // command line parameter
            FileInputStream fStream = new FileInputStream("E:\\Probleme Info\\HTML Analizer\\src\\com\\Cristi\\textfile.txt");
            // Get the object of DataInputStream
            DataInputStream in = new DataInputStream(fStream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine, input = "";
            int line_counter=0;
            //Read File Line By Line
            if((strLine = br.readLine()) != null) {
                tmp = "{TOKEN NO. , TOKEN , NAME , TYPE/VALUE , PARENT TAG ,LINE NO. }" + "\n";
                tmp += "========================================================" + "\n" ;
            }
            else {
                tmp += "-------Empty file---------\n";
            }
            while ((input = br.readLine()) != null)   {
                // Print the content on the console
                //System.out.println (strLine);
                ArrayList<Token> tokenList = getToken(input);
                line_counter+=1;
                verifyTokens(tokenList,line_counter);
            }
            System.out.println(tmp);
            System.out.println(error);
            //Close the input stream
            in.close();
        }catch (Exception e){//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }

    }

    public static void verifyTokens(ArrayList<Token> vectorTag, int line_counter ) {
        String att_parent="--";
        MyTags T = new MyTags();
        for(int i = 0; i < vectorTag.size(); i++) {

            // daca avem un tag in vector, salvam numele tagului in cazul in care dupa el urmeaza un atribut
            if(vectorTag.get(i).getToken_type()== "TAG" && vectorTag.get(i).getTag_type_or_attr_value()== "OT") {
                att_parent = vectorTag.get(i).getToken_name();
            }

            // daca avem un atribut
            if	(vectorTag.get(i).getToken_type()== "ATT" ) {

                // cautam numele tagului in Stringul cu taguri si atribute
                // val atributului trebuie sa fie diferita de null
                if (T.findAttr(vectorTag.get(i).getToken_name()) && vectorTag.get(i).getTag_type_or_attr_value()!= "null" ) {
                    tmp += "{ " +count+ " , " + vectorTag.get(i).getToken_type() +" , "+vectorTag.get(i).getToken_name() + " , "+vectorTag.get(i).getTag_type_or_attr_value() + " , " +att_parent+ " , "+line_counter+ " } " + "\n";
                    tmp += "--------------------------------------------------------------------------------" + "\n" ;
                    count++;
                }

                // daca val atributului este null, apare o eroare
                else if (vectorTag.get(i).getTag_type_or_attr_value() == "null") {
                    error += " @line "+line_counter+"  : Token '"+vectorTag.get(i).getToken_name() +"' Value Can't be 'null'." + "\n";
                    errorCounter ++;
                }

                // daca numele atributului nu apare in lista de atribute ale tagului curent genereaza eroare
                else {
                    error += " @line "+line_counter+"  : '"+vectorTag.get(i).getToken_name() +"' is not allowed as Attribute of '"+att_parent+"' TAG." + "\n";
                    errorCounter ++;
                }
            }
            else if(vectorTag.get(i).getToken_type()== "TAG") {
                if (T.findTag(vectorTag.get(i).getToken_name())) {
                    tmp += "{ " +count+ " , " + vectorTag.get(i).getToken_type() +" , "+vectorTag.get(i).getToken_name() + " , "+vectorTag.get(i).getTag_type_or_attr_value() + " , "+line_counter+ " } " + "\n";
                    tmp += "--------------------------------------------------------------------------------" + "\n" ;
                    count++;
                }
                else {
                    error += " @line "+line_counter+"  : '"+vectorTag.get(i).getToken_name() +"' is an UnExpected Token." + "\n";
                    errorCounter ++;
                }
            }

        }
    }



    // parcurgem textul caracter cu caracter
    public static ArrayList<Token> getToken(String input) {

        // lexeme = numele tagului / atributului
        // value = valoarea atribultului
        String lexeme = "" , value = "";
        Token token = null;
        ArrayList<Token> tokenArrayList = new ArrayList<Token>();
        int d=0;
        int j=0,k=0;
        for (int i = 0; i < input.length(); i++) {

            // Un tag incepe mereu cu < deci asta este prima conditie
            if (input.charAt(i) == '<') {

                // daca urmatorul caracter dupa  < este litera, atunci incepe un tag
                i++;
                if (isALetter(input.charAt(i))) {

                    //salvam caracterele parcurse pana cand gasim un > sau un spatiu
                    lexeme = "" + input.charAt(i);
                    i++;
                    while (input.charAt(i) != '>' && input.charAt(i) != ' ') {
                        lexeme += input.charAt(i);

                        // daca gasim un spatiu inseamna ca acel tag are si un atribut
                        // folosim un alt iterator pentru a parcurge caracterele atributului (j)
                        i++;
                        j=i;
                    }
                    // cautam caracterul > care reprezinta incheierea tagului
                    while (input.charAt(i) != '>') {
                        i++;
                        k=i;
                    }

                    //daca gasim un sir de caractere de forma unui tag, creem un obiect de tip Token pe care il adaugam intr-o lista
                    // ulterior, cu metoda GenerateTokens vor verifica daca acesti Token sunt taguri cunoscute din HTML
                    token = new Token("TAG", lexeme,"OT");
                    tokenArrayList.add(token);

                    // Atribut

                    //Numele si val atributului trebuie sa fie intre locatie unde s-a terminat tagul (j) si locatia unde apare caracterul > (k)
                    for (int z = j; z < k; z++) {
                        while (input.charAt(z) == ' ') {
                            z++;
                            if (isALetter(input.charAt(z))) {
                                lexeme = "" + input.charAt(z);
                                z++;

                                // Atribute name

                                // cand ajungem la un caracter de tipul = sau spatiu inseamna ca s-a terminat numele atributului
                                while (input.charAt(z) != '=' && input.charAt(z) != ' ') {
                                    lexeme += input.charAt(z);
                                    z++;
                                }

                                // daca gasim " inseamna ca incepe valoarea atributului
                                while (input.charAt(z) != '"') {
                                    z++;
                                }

                                // Incepe attribute value
                                if (input.charAt(z) == '"') {
                                    z++;
                                    if(isALetter(input.charAt(z)) || isAtt_value(input.charAt(z)) ) {
                                        value = "" + input.charAt(z);
                                        z++;

                                        // daca gsaim din nou " inseamna ca se termina val atributului
                                        while( (isALetter(input.charAt(z)) || isAtt_value(input.charAt(z))) && input.charAt(z)!= '"') {
                                            value += input.charAt(z);
                                            z++;
                                        }
                                    }

                                    else if (input.charAt(z) == '"') {
                                        value="null";
                                    }

                                }

                                //daca intre un sir de caracter de tip tag gasim un sir de caractere de forma unui atribut, creem un obiect de tip Token pe care il adaugam intr-o lista
                                // ulterior, cu metoda GenerateTokens vor verifica daca acesti Token sunt atribute ale vreunui tag cunoscute din HTML
                                // tokenul creat va contine: tipul de token, numele atributului si valoarea atributului
                                token = new Token("ATT", lexeme,value);
                                tokenArrayList.add(token);
                            }
                        }
                    }

                }

                // se inchide tag-ul
                // daca dupa < urmeaza / inseamna ca urmeaza un closing tag
                if (input.charAt(i) == '/') {
                    i++;
                    if (isALetter(input.charAt(i))) {
                        lexeme = "" + input.charAt(i);
                        i++;

                        // salvam toate caracterele pana cand gasim un > care inseamnca ca se termina tagul
                        while (input.charAt(i) != '>' && input.charAt(i) != ' ') {
                            lexeme += input.charAt(i);
                            i++;

                        }

                        token = new Token("TAG", lexeme,"CT");
                        tokenArrayList.add(token);

                    }
                }
            }
        }
        return tokenArrayList;
    }

    public static boolean isALetter(char inputChar) {
        Boolean itIsALetter = false;
        if("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM".indexOf(inputChar) != -1) {
            itIsALetter = true;
        }
        return itIsALetter;
    }

    public static boolean isANumber(char inputChar) {
        Boolean itIsANumber = false;
        if("1234567890".indexOf(inputChar) != -1) {
            itIsANumber = true;
        }
        return itIsANumber;
    }

    public static boolean isAtt_value(char inputChar) {
        Boolean itIsAtt_value = false;
        if("1234567890".indexOf(inputChar) != -1 || "_:#-.!/ ".indexOf(inputChar) != -1) {
            itIsAtt_value = true;
        }
        return itIsAtt_value;
    }


}
