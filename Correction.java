package com.Cristi;

public class Correction {

    int pos,total_matches,length;
    String tag_name;

    public Correction(int pos,String tag_name,int total_matches,int length) {
        this.pos = pos;
        this.tag_name = tag_name;
        this.total_matches= total_matches;
        this.length = length;
    }



    public int getPos() {
        return pos;
    }

    public String getTag_name() {
        return tag_name;
    }

    public int getTotal_matches() {
        return total_matches;
    }

    public int getLength() {
        return length;
    }
}
