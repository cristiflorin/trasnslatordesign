import java.util.ArrayList;

public class MyTags {

    /*
    cautam tagul in vectorul de taguri si atribute
    tagul se afla mereu pe pozitia 0
    daca il gaseste facem break si returneaza true
    daca ajunge la final si nu l-a gasit returneaza false

    */

    public static boolean findTag(String term) {
        boolean result;
        int row = tags.length;

        outer_loop:
        for (int i=0; i<tags.length; i++) {
            if (term.equals(tags[i][0])) {
                row = i;
                break outer_loop;
            }

        }

        if (row == tags.length) {
            result =false;
        }
        else {
            result =true;
        }

        return result;
    }


    public static boolean findAttr(String term) {
        boolean result;
        int row = tags.length;

        outer_loop:
        for (int i=0; i<tags.length; i++) {
            for (int j=1; j<7; j++) {
                if (term.equals(tags[i][j])) {
                    row = i;
                    break outer_loop;
                }
            }
        }

        if (row == tags.length) {
            result =false;
        }
        else {
            result =true;
        }

        return result;
    }

    public static int findParentPos(String parent){
        int pos=0;

        outer_loop:
        for (int i=0; i<tags.length; i++) {
            if (parent.equals(tags[i][0])) {
                pos=i;
                break outer_loop;
            }
        }

        return pos;
    }

   

    static String[][] tags = new String [][] {
            { "html", "lang","", "", "1",  "", "P" },
            { "!DOCTYPE", "","", "", "",  "", "" },
            { "&xxx", "","", "", "",  "", "" },
            { "abbr", "","", "", "",  "", "" },
            { "acronym", "","", "", "",  "", "" },
            { "address", "","", "", "",  "", "" },
            { "applet", "","", "", "",  "", "" },
            { "area", "shape","coords", "href", "alt",  "nohref", "" },
            { "b", "","", "", "",  "", "" },
            { "basefont", "size","", "", "",  "", "" },
            { "base", "href","", "", "",  "", "" },
            { "bdo", "","", "", "",  "", "" },
            { "big", "","", "", "",  "", "" },
            { "blockquote", "","", "", "",  "", "" },
            { "body", "bgcolor","background", "text", "link",  "vlink", "alink" },
            { "br", "clear","", "", "",  "", "" },
            { "button", "","", "", "",  "", "" },
            { "caption", "","", "", "",  "", "" },
            { "center", "","", "", "",  "", "" },
            { "cite", "","", "", "",  "", "" },
            { "code", "","", "", "",  "", "" },
            { "col", "","", "", "",  "", "" },
            { "colgroup", "","", "", "",  "", "" },
            { "dd", "","", "", "",  "", "" },
            { "del", "","", "", "",  "", "" },
            { "dfn", "","", "", "",  "", "" },
            { "dir", "compact","", "", "",  "", "" },
            { "div", "align","", "", "",  "", "" },
            { "dl", "compact","", "", "",  "", "" },
            { "em", "","", "", "",  "", "" },
            { "fieldset", "","", "", "",  "", "" },
            { "font", "size","color", "", "",  "", "" },
            { "form", "action","method", "enctype", "",  "", "" },
            { "frame", "","", "", "",  "", "" },
            { "frameset", "","", "", "",  "", "" },
            { "head", "","", "", "",  "", "" },
            { "hn", "align","size", "width", "noshade",  "", "" },
            { "html", "version","", "", "",  "", "" },
            { "i", "","", "", "",  "", "" },
            { "iframe", "","", "", "",  "", "" },
            { "img", "src","align", "width", "height",  "border", "usemap" },
            { "input", "type","name", "value", "checked",  "src", "size" },
            { "ins", "","", "", "",  "", "" },
            { "isindex", "prompt","", "", "",  "", "" },
            { "kbd", "","", "", "",  "", "" },
            { "label", "","", "", "",  "", "" },
            { "legend", "","", "", "",  "", "" },
            { "li", "","", "", "",  "", "" },
            { "link", "","", "", "",  "", "" },
            { "listing", "","", "", "",  "", "" },
            { "map", "name","", "", "",  "", "" },
            { "menu", "compact","", "", "",  "", "" },
            { "meta", "name","http-equiv", "content", "",  "", "" },
            { "nextid", "n","", "", "",  "", "" },
            { "noframes", "","", "", "",  "", "" },
            { "noscript", "","", "", "",  "", "" },
            { "object", "","", "", "",  "", "" },
            { "ol", "","", "", "",  "", "" },
            { "optgroup", "","", "", "",  "", "" },
            { "option", "selected","value", "", "",  "", "" },
            { "p", "align","", "", "",  "", "" },
            { "param", "","", "", "",  "", "" },
            { "plaintext", "","", "", "",  "", "" },
            { "pre", "width","", "", "",  "", "" },
            { "q", "","", "", "",  "", "" },
            { "s", "","", "", "",  "", "" },
            { "samp", "","", "", "",  "", "" },
            { "script", "","", "", "",  "", "" },
            { "select", "name","size", "multiple", "",  "", "" },
            { "small", "","", "", "",  "", "" },
            { "span", "","", "", "",  "", "" },
            { "strike", "","", "", "",  "", "" },
            { "strong", "","", "", "",  "", "" },
            { "style", "","", "", "",  "", "" },
            { "sub", "","", "", "",  "", "" },
            { "sup", "","", "", "",  "", "" },
            { "table", "align","border", "width", "cellspacing",  "cellpadding", "" },
            { "tbody", "","", "", "",  "", "" },
            { "td", "align","rowspan", "colspan", "width",  "height", "nowrap" },
            { "textarea", "name","rows", "cols", "",  "", "" },
            { "th", "align","rowspan", "colspan", "width",  "height", "valign" },
            { "thead", "","", "", "",  "", "" },
            { "title", "","", "", "",  "", "" },
            { "tt", "","", "", "",  "", "" },
            { "u", "","", "", "",  "", "" },
            { "ul", "type","compact", "", "",  "", "" },
            { "var", "","", "", "",  "", "" },
            { "xmp", "","", "", "",  "", "" },
            { "tr", "align","valign", "", "",  "", "" },
            { "a", "name","href", "rel", "rev",  "title", "methods" }
    };
}
